//环境控制: 设置 true 开https
const httpsversion = false;   //开关 https:// 改成http://

//竟然找不到包函数： http://nodejs.cn/api/http2/class_http2secureserver.html
const http2 = require('http2')  //客户端都不支持啊unencrypted HTTP/2都报错
//证书，加域名 ；没有证书的话就 只能用这个
const http = require('http')

const fs = require('fs')
const path = require('path')
const Koa = require('koa');
const KoaSinglePage = require('koa-single-page');


const app = new Koa();

//const static = require('koa-static');
// 配置静态web服务的中间件
//app.use(static(path.join( __dirname, './build')));
//前端静态资源的根目录 web服务。
//要 支持SPA方式； static-spa  history-api
// http2服务端代码  koa-static 不支持SPA的URL直接输入刷新方式访问！

//前端build输出的 打包文件:目录固定 /build/**
//上传和拷贝前端
app.use(KoaSinglePage('build'));

app.use(ctx => {
    ctx.type = 'html';
    ctx.body = fs.createReadStream('./index.html');
})

//todo: HTTPS HTTP2 证书，加域名 版本。
//服务器 CA认证！ 代码都找不到： 看文档 http://nodejs.cn/api/http2.html

const server = httpsversion ?
    http2.createSecureServer({
        cert: fs.readFileSync(path.join(__dirname, "./certs/server.crt")),
        key: fs.readFileSync(path.join(__dirname, "./certs/server.key"))
    }, app.callback())
    :
    http.createServer({}, app.callback());


//端口，IP, 域名。
//格局域名IP：端口 组合来区分前端网站。

const port = process.env.port || 4765;

server.listen(port, (err) => {
    console.log(`支持SPA的前端 源代码资源 服务器 listening on ${port}, 用${httpsversion ? 'HTTPS' : 'HTTP'}访问`)

})

// const s = () => {
//
//
//     [ {
//         "BASE_INFO" : {
//             "ON_DEMAND_STA" : 0,
//             "EQP_TYPE" : "2000",
//
//         },
//         "PARAMS" : {
//             "VESSEL_PARAM" : {
//                 "OIDNO" : "RC72995",
//                 "CONTAINERVOLUME" : "0"
//             }
//         }
//     } ]
//
// };

// <JumpMeasure tag={'Measure'} rep={rep}>八、观测数据及测量结果</JumpMeasure>
// <RepLink rep={rep} tag={'Survey'}>
// </RepLink>
//
//
//     <div>
//         {op && <JumpPref rep={rep} tag={'ExplosionProof'}>附表1 测量结果记录下</JumpPref>}
//     </div>
//
// {mxTemp && config温度试.map(([title,tag,]:any, c:number) => {
//     return <div key={c} css={{display: 'contents'}}>{title} 最高温= {mxTemp[c]} ；</div>
// })}